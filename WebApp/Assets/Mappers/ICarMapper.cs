﻿namespace WebApp.Assets.Mappers
{
    public interface ICarMapper
    {
        WebApp.Models.Car MapToView(DataContext.Models.Car proto);
        DataContext.Models.Car MapToDbItem(WebApp.Models.Car proto);
    }
}
