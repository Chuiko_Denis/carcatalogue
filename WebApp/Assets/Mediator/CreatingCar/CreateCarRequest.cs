﻿using System;
using MediatR;
using WebApp.Models;

namespace WebApp.Assets.Mediator.CreatingCar
{
    public class CreateCarRequest : IRequest<Guid>
    {
        public Car CarPrototype { get; internal set; }
    }
}
