﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApp.Assets.Mediator.CreatingCar;
using WebApp.Assets.Mediator.DeletingCar;
using WebApp.Assets.Mediator.GettingCarById;
using WebApp.Assets.Mediator.GettingNotDeletedCarsList;
using WebApp.Assets.Mediator.UpdatingCar;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class CarsController : Controller
    {
        readonly IMediator _mediator;

        public CarsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<ActionResult> Index()
        {
            var request = new GetNotDeletedCarsListRequest();
            var model = await _mediator.Send(request);

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Car newCar)
        {
            var createCarRequest = new CreateCarRequest
            {
                CarPrototype = newCar
            };

            await _mediator.Send(createCarRequest);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<ActionResult> Edit(Guid carId)
        {
            var request = new GetCarByIdRequest {CarId = carId};
            var model = await _mediator.Send(request);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit([FromForm] Car updatedCar)
        {
            var request = new UpdateCarRequest {CarNewVersion = updatedCar};
            await _mediator.Send(request);
            
            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Delete(Guid carId)
        {
            var request = new DeleteCarRequest {CarId = carId};
            await _mediator.Send(request);

            return RedirectToAction("Index");
        }
    }
}
