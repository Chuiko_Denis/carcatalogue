﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Car
    {
        public Guid Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is required.")]
        [RegularExpression(@"^\w[\w\s]{0,254}\w$", ErrorMessage = "Car Name should have a length from 2 to 256 symbols without wrapping by spaces.")]
        [Display(Name = "Car Name")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Year is required.")]
        public int Year { get; set; }

        [EnumDataType(typeof(CarProducersEnum))]
        public CarProducersEnum Producer { get; set; }
    }
}
