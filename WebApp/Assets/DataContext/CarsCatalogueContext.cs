﻿using Microsoft.EntityFrameworkCore;
using WebApp.Assets.DataContext.Models;

namespace WebApp.Assets.DataContext
{
    public partial class CarsCatalogueContext : DbContext
    {
        public CarsCatalogueContext()
        {
        }

        public CarsCatalogueContext(DbContextOptions<CarsCatalogueContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Car> Car { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
