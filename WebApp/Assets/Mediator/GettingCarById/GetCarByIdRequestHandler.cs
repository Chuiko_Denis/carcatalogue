﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using WebApp.Assets.DataContext;
using WebApp.Assets.Mappers;
using WebApp.Models;

namespace WebApp.Assets.Mediator.GettingCarById
{
    public class GetCarByIdRequestHandler : IRequestHandler<GetCarByIdRequest, Car>
    {
        readonly ILogger<GetCarByIdRequestHandler> _logger;
        readonly ICarsRepository _carRepository;
        readonly ICarMapper _carMapper;

        public GetCarByIdRequestHandler(
            ILogger<GetCarByIdRequestHandler> logger, 
            ICarsRepository carRepository, 
            ICarMapper carMapper)
        {
            _logger = logger;
            _carRepository = carRepository;
            _carMapper = carMapper;
        }

        public async Task<Car> Handle(GetCarByIdRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var targetCarFromDb = await _carRepository.GetCarAsync(request.CarId);
                return _carMapper.MapToView(targetCarFromDb);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Can't get data about car with id {request.CarId}", ex);

                throw;
            }
        }
    }
}
