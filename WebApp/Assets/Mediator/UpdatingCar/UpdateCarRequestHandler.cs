﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebApp.Assets.DataContext;
using WebApp.Assets.Mappers;

namespace WebApp.Assets.Mediator.UpdatingCar
{
    public class UpdateCarRequestHandler : IRequestHandler<UpdateCarRequest>
    {
        readonly ILogger<UpdateCarRequestHandler> _logger;
        readonly ICarsRepository _carRepository;
        readonly ICarMapper _carMapper;

        public UpdateCarRequestHandler(
            ILogger<UpdateCarRequestHandler> logger, 
            ICarsRepository carRepository, 
            ICarMapper carMapper)
        {
            _logger = logger;
            _carRepository = carRepository;
            _carMapper = carMapper;
        }

        public async Task<Unit> Handle(UpdateCarRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var carNewVersionInDbFormat = _carMapper.MapToDbItem(request.CarNewVersion);
                await _carRepository.UpdateCarAsync(carNewVersionInDbFormat);

                return Unit.Value;
            }
            catch (Exception ex)
            {
                var carJson = JsonConvert.SerializeObject(request.CarNewVersion);
                _logger.LogError($"Can't update data for new version of data of car: {carJson}");

                throw;
            }
        }
    }
}
