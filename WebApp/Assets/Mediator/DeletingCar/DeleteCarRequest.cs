﻿using System;
using MediatR;

namespace WebApp.Assets.Mediator.DeletingCar
{
    public class DeleteCarRequest : IRequest
    {
        public Guid CarId { get; set; }
    }
}
