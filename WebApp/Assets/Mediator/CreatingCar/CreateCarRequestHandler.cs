﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebApp.Assets.DataContext;
using WebApp.Assets.Mappers;

namespace WebApp.Assets.Mediator.CreatingCar
{
    public class CreateCarRequestHandler : IRequestHandler<CreateCarRequest, Guid>
    {
        readonly ILogger<CreateCarRequestHandler> _logger;
        readonly ICarsRepository _carRepository;
        readonly ICarMapper _carMapper;

        public CreateCarRequestHandler(
            ILogger<CreateCarRequestHandler> logger, 
            ICarsRepository carRepository, 
            ICarMapper carMapper)
        {
            _logger = logger;
            _carRepository = carRepository;
            _carMapper = carMapper;
        }

        public async Task<Guid> Handle(CreateCarRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var dataContextCar = _carMapper.MapToDbItem(request.CarPrototype);
                var createCarId = await _carRepository.CreateCarAsync(dataContextCar);

                return createCarId;
            }
            catch (Exception ex)
            {
                var carJson = JsonConvert.SerializeObject(request.CarPrototype);
                _logger.LogError($"Creating a car failed with parameters: {carJson}");

                throw;
            }
        }
    }
}
