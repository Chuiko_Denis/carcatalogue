﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public enum CarProducersEnum
    {
        [Display(Name = "Enum Vw")]
        EnumVw = 10, 

        [Display(Name = "Audi")]
        Audi = 20, 

        [Display(Name = "Alfa Romeo")]
        AlfaRomeo = 30
    }
}
