﻿using System;
using MediatR;
using WebApp.Models;

namespace WebApp.Assets.Mediator.GettingCarById
{
    public class GetCarByIdRequest : IRequest<Car>
    {
        public Guid CarId { get; set; }
    }
}
