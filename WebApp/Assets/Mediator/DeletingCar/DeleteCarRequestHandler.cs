﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using WebApp.Assets.DataContext;

namespace WebApp.Assets.Mediator.DeletingCar
{
    public class DeleteCarRequestHandler : IRequestHandler<DeleteCarRequest>
    {
        readonly ILogger<DeleteCarRequestHandler> _logger;
        readonly ICarsRepository _carRepository;

        public DeleteCarRequestHandler(
            ILogger<DeleteCarRequestHandler> logger, 
            ICarsRepository carRepository)
        {
            _logger = logger;
            _carRepository = carRepository;
        }

        public async Task<Unit> Handle(DeleteCarRequest request, CancellationToken cancellationToken)
        {
            try
            {
                await _carRepository.DeleteCarAsync(request.CarId);

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Deletion of Car with id {request.CarId} was failed.", ex);

                throw;
            }
        }
    }
}
