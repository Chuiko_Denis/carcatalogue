﻿using System;

namespace WebApp.Assets.DataContext.Models
{
    public partial class Car
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }
        public int? ProducerId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
