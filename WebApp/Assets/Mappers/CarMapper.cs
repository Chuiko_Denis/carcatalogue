﻿using WebApp.Models;

namespace WebApp.Assets.Mappers
{
    public class CarMapper : ICarMapper
    {
        public Car MapToView(DataContext.Models.Car proto)
        {
            return new Car
            {
                Id = proto.Id,
                Name = proto.Name,
                Description = proto.Description,
                Year = proto.Year,
                Producer = (CarProducersEnum) proto.ProducerId
            };
        }

        public DataContext.Models.Car MapToDbItem(Car proto)
        {
            return new DataContext.Models.Car
            {
                Id = proto.Id,
                Name = proto.Name,
                Description = proto.Description,
                Year = proto.Year,
                ProducerId = (int) proto.Producer,
                IsDeleted = false
            };
        }
    }
}
