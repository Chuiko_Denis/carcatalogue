﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Assets.DataContext.Models;

namespace WebApp.Assets.DataContext
{
    public interface ICarsRepository
    {
        Task<Car> GetCarAsync(Guid id);
        Task<IList<Car>> GetNotDeletedCarsAsync();
        Task<Guid> CreateCarAsync(Car newCar);
        Task UpdateCarAsync(Car car);
        Task DeleteCarAsync(Guid carId);
    }
}
