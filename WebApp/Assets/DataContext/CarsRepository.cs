﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApp.Assets.DataContext.Models;

namespace WebApp.Assets.DataContext
{
    public class CarsRepository : ICarsRepository
    {
        readonly CarsCatalogueContext _dbContext;

        public CarsRepository(CarsCatalogueContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Car> GetCarAsync(Guid id)
        {
            if (await _dbContext.Car.FindAsync(id) is { } theCar)
                return theCar;

            throw new Exception($"There is no registered car with Id \"{id}\".");
        }

        public async Task<IList<Car>> GetNotDeletedCarsAsync()
        {
            return await _dbContext.Car.Where(c => !c.IsDeleted).ToListAsync();
        }

        public async Task<Guid> CreateCarAsync(Car newCar)
        {
            var result = (await _dbContext.Car.AddAsync(newCar)).Entity;
            await _dbContext.SaveChangesAsync();

            return result.Id;
        }

        public async Task UpdateCarAsync(Car car)
        {
            var carToUpdate = await _dbContext.Car.FindAsync(car.Id);

            if (carToUpdate == null)
                return;

            carToUpdate.Name = car.Name;
            carToUpdate.Description = car.Description;
            carToUpdate.ProducerId = car.ProducerId;
            carToUpdate.Year = car.Year;

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteCarAsync(Guid carId)
        {
            if ((await _dbContext.Car.FindAsync(carId)) is { } theCar)
            {
                theCar.IsDeleted = true;
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
