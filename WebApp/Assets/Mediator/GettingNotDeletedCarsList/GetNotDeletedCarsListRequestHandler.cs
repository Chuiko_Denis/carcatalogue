﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using WebApp.Assets.DataContext;
using WebApp.Assets.Mappers;
using WebApp.Models;

namespace WebApp.Assets.Mediator.GettingNotDeletedCarsList
{
    public class GetNotDeletedCarsListRequestHandler : IRequestHandler<GetNotDeletedCarsListRequest, IList<Car>>
    {
        readonly ILogger<GetNotDeletedCarsListRequestHandler> _logger;
        readonly ICarsRepository _carRepository;
        readonly ICarMapper _carMapper;

        public GetNotDeletedCarsListRequestHandler(
            ILogger<GetNotDeletedCarsListRequestHandler> logger, 
            ICarsRepository carRepository, 
            ICarMapper carMapper)
        {
            _logger = logger;
            _carRepository = carRepository;
            _carMapper = carMapper;
        }

        public async Task<IList<Car>> Handle(GetNotDeletedCarsListRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var carsFromDb = await _carRepository.GetNotDeletedCarsAsync();
                var transformedCars = carsFromDb.Select(_carMapper.MapToView);

                return transformedCars.ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Getting not deleted cars list failed.", ex);

                throw;
            }
        }
    }
}
