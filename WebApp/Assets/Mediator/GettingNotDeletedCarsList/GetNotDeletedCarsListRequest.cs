﻿using System.Collections.Generic;
using MediatR;
using WebApp.Models;

namespace WebApp.Assets.Mediator.GettingNotDeletedCarsList
{
    public class GetNotDeletedCarsListRequest : IRequest<IList<Car>>
    {
    }
}
