﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Assets.DataContext;
using WebApp.Assets.Mappers;

namespace WebApp.AppStart.Services
{
    public static class ServicesExtensions
    {
        public static IServiceCollection RegisterAppServices(
            this IServiceCollection serviceCollection, 
            IConfiguration configuration)
        {
            serviceCollection.AddDbContext<CarsCatalogueContext>(options =>
            {
                var connectionString = configuration.GetConnectionString("CarsCatalogueDataBase");
                options.UseSqlServer(connectionString);
            });
            serviceCollection.AddTransient<ICarMapper, CarMapper>();
            serviceCollection.AddTransient<ICarsRepository, CarsRepository>();

            return serviceCollection;
        }
    }
}
