﻿using MediatR;
using WebApp.Models;

namespace WebApp.Assets.Mediator.UpdatingCar
{
    public class UpdateCarRequest : IRequest
    {
        public Car CarNewVersion { get; set; }
    }
}
